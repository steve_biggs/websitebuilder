"""
Represents a pseudo-HTML file

Copyright 2018 Steve Biggs

This file is part of websitebuilder.

websitebuilder is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

websitebuilder is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with websitebuilder.  If not, see http://www.gnu.org/licenses/.
"""

import re
from HtmlFile import HtmlFile
from PseudoHtmlBlock import PseudoHtmlBlock as Block
from PseudoHtmlLine import PseudoHtmlLine as Line
from Toc import Toc


class PseudoHtmlFile(HtmlFile):

    def __init__(self, outputDir, contentPath):
        super(PseudoHtmlFile, self).__init__(outputDir, contentPath)
        self.outputFilename = re.sub('.shtml', '.html', contentPath.split('/')[-1])
        self.block = Block()
        self.toc = None

    def parsePseudoHtml(self):
        self.output = []
        # Process first line to get it into the block but don't record the
        # output as this relates to the non-existent zeroth line
        self.parsePseudoHtmlLine(self.contents[0])
        # Loop over the rest of the lines and record the output of processing
        # each (including an extra blank line at the end so that the last line
        # does not get missed
        for lineText in self.contents[1:] + ['']:
            self.output.append(self.parsePseudoHtmlLine(lineText))

    def parsePseudoHtmlLine(self, lineText):
        line = self.textToLine(lineText)
        self.block.step(line)
        processedLine = self.block.parsePseudoHtml()
        return processedLine.text

    def captureTocEntries(self):
        tocEntries = []
        for lineText in self.contents:
            line = self.textToLine(lineText)
            line.captureTocEntry()
            if line.tocEntry is not None:
                tocEntries.append(line.tocEntry)
        self.toc = self.newToc(tocEntries)

    def textToLine(self, text):
        return Line(text)

    def newToc(self, tocEntries):
        return Toc(tocEntries)
