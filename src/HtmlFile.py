"""
Base class representing HTML files in this project

Copyright 2018 Steve Biggs

This file is part of websitebuilder.

websitebuilder is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

websitebuilder is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with websitebuilder.  If not, see http://www.gnu.org/licenses/.
"""


class HtmlFile:

    def __init__(self, outputDir, contentPath):
        self.outputDir = outputDir
        self.contentPath = contentPath
        self.contents = None
        self.outputFilename = None
        self.output = None

    def read(self):
        with open(self.contentPath, 'r') as f:
            self.contents = f.readlines()

    def writeOutput(self):
        with open(self.outputDir + '/' + self.outputFilename, 'w') as f:
            for line in self.output:
                f.write(line)
