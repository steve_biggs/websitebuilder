"""
Represents a the template file for a web page

Copyright 2018 Steve Biggs

This file is part of websitebuilder.

websitebuilder is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

websitebuilder is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with websitebuilder.  If not, see http://www.gnu.org/licenses/.
"""

import re
from HtmlTemplateFile import HtmlTemplateFile


class PageTemplateFile(HtmlTemplateFile):

    def __init__(self, outputDir, contentPath, title, toc):
        super(PageTemplateFile, self).__init__(outputDir, contentPath, title)
        self.toc = toc
        self.outputFilename = self.getSelfPageName()
        if self.outputFilename == 'index':
            self.outputFilename += '.html'
        self.pages = [  # TODO - generalise this as this is project specific (config file?)
                      'Home',
                      'Introduction',
                      'LO1',
                      'LO2',
                      'LO3',
                      'LO4',
                      'LO5',
                      'LO6',
                      'Conclusions',
                      'Bibliography',
                      'Appendix - UKPSF']

    def insertVariables(self):
        self.output = []
        replace = r'VAR_REPLACE_'
        for line in self.contents:
            line = re.sub(replace + r'1', self.title, line)
            line = re.sub(replace + r'2', self.getBarPreviousLine(), line)
            line = re.sub(replace + r'3', self.getSelfPageName(), line)
            line = re.sub(replace + r'4', self.getBarNextLine(), line)
            line = re.sub(replace + r'5', self.getDropPreviousLine(), line)
            line = re.sub(replace + r'6', self.getDropNextLine(), line)
            line = re.sub(replace + r'7', self.getPageLink(), line)
            line = re.sub(replace + r'8', self.toc.getHtml(), line)
            self.output.append(line)

    def getBarPreviousLine(self):
        return self.getPreviousLine(isBar=True)

    def getBarPreviousPrefix(self):
        return '<a class="w3-bar-item w3-button w3-hover-orange w3-hide-small" href="'

    def getPreviousLine(self, isBar):
        return self.getNavLine(isBar=isBar, isPrev=True)

    def getNavLine(self, isBar, isPrev):
        filename = self.getNavFilename(isPrev)
        if filename is None:
            navLine = self.getComment(isPrev)
        else:
            navLine = self.getPrefix(isBar, isPrev) + filename + self.getSuffix(isPrev)
        return navLine

    def getNavFilename(self, isPrev):
        if isPrev:  # TODO - this if logic is repeated in next method which suggests this should be split off into polymorphic subclasses
            filename = self.getPreviousFilename()
        else:
            filename = self.getNextFilename()
        return filename

    def getComment(self, isPrev):
        if isPrev:
            comment = '<!-- No previous on homepage -->'
        else:
            comment = '<!-- No next on last page -->'
        return comment

    def getPrefix(self, isBar, isPrev):
        if isBar and isPrev:
            prefix = self.getBarPreviousPrefix()
        elif isBar and not isPrev:
            prefix = self.getBarNextPrefix()
        elif not isBar and isPrev:
            prefix = self.getDropPreviousPrefix()
        else:
            prefix = self.getDropNextPrefix()
        return prefix

    def getSuffix(self, isPrev):
        if isPrev:
            suffix = self.getPreviousSuffix()
        else:
            suffix = self.getNextSuffix()
        return suffix

    def getPreviousFilename(self):
        return self.getFilenameViaOffset(isPrev=True)

    def getFilenameViaOffset(self, isPrev):
        index = self.getPageIndex()
        badIndex = 0 if isPrev else len(self.pages) - 1
        if index == badIndex:
            filename = None
        else:
            offset = -1 if isPrev else +1
            title = self.pages[index + offset]
            filename = self.getPageName(title)
        return filename

    def getPageIndex(self):
        return self.pages.index(self.title)

    def getPreviousSuffix(self):
        return '">Previous</a>'

    def getBarNextLine(self):
        return self.getNextLine(isBar=True)

    def getBarNextPrefix(self):
        return '<a class="w3-bar-item w3-button w3-hover-orange w3-hide-small w3-hide-medium" href="'

    def getNextLine(self, isBar):
        return self.getNavLine(isBar=isBar, isPrev=False)

    def getNextSuffix(self):
        return '">Next</a>'

    def getNextFilename(self):
        return self.getFilenameViaOffset(isPrev=False)

    def getDropPreviousLine(self):
        return self.getPreviousLine(isBar=False)

    def getDropPreviousPrefix(self):
        return '<a class="w3-bar-item w3-button w3-hover-orange w3-hide-medium" href="'

    def getDropNextLine(self):
        return self.getNextLine(isBar=False)

    def getDropNextPrefix(self):
        return '<a class="w3-bar-item w3-button w3-hover-orange" href="'

    def getPageLink(self):
        pageName = self.getSelfPageName()
        if pageName == 'index':
            pageLink = '.'
        else:
            pageLink = pageName
        return pageLink
