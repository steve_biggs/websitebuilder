"""
Represents a block of three lines from a pseudo-HTML file

Copyright 2018 Steve Biggs

This file is part of websitebuilder.

websitebuilder is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

websitebuilder is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with websitebuilder.  If not, see http://www.gnu.org/licenses/.
"""

from PseudoHtmlLine import PseudoHtmlLine as Line


class PseudoHtmlBlock:

    def __init__(self, before=Line(), this=Line(), after=Line()):
        self.before = before
        self.this = this
        self.after = after

    def parsePseudoHtml(self):
        output = self.this
        if self.isStartOfParagraph():
            output = output.openParagraph()
        if self.isEndOfParagraph():
            output = output.closeParagraph()
        if self.isBookmark():
            output = output.formatBookmark()
        return output

    def step(self, newAfter):
        self.before = self.this
        self.this = self.after
        self.after = newAfter

    def isStartOfParagraph(self):
        return self.before.isBlank() and self.isParagraphBoundaryCandidate()

    def isParagraphBoundaryCandidate(self):
        return not self.this.isBlank() and not self.this.isHeader() \
            and not self.this.isList()

    def isEndOfParagraph(self):
        return self.after.isBlank() and self.isParagraphBoundaryCandidate()

    def isBookmark(self):
        return self.this.isBookmark()
