"""
Main class of websitebuilder

Copyright 2018 Steve Biggs

This file is part of websitebuilder.

websitebuilder is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

websitebuilder is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with websitebuilder.  If not, see http://www.gnu.org/licenses/.
"""

import sys
import argparse
from WebPage import WebPage
from Arg import Arg


class WebsiteBuilder:

    def __init__(self):
        self.parsedArgs = None
        self.options = [  # Define options here to avoid duplication between createParser and buildTarget
            self.newArg('outputDir', 1, helpText='Directory in which to place output files. OUTPUTDIR = path to output directory.'),
            self.newArg('content', 1),
            self.newArg('page', 2),
            self.newArg('bar', 2),
            self.newArg('drop', 2)]

    def main(self, *args):
        parser = self.createParser()
        self.parsedArgs, dummy = parser.parse_known_args(args)
        self.buildTarget()

    def createParser(self):
        parser = argparse.ArgumentParser()
        for opt in self.options:  # Loop over options defined in init and add them to the parser
            parser.add_argument('--' + opt.name, nargs=opt.narg, help=opt.help)
        return parser

    def buildTarget(self):
        if self.parsedArgs is not None:  # Cannot build if arguments have not been parsed yet
            for opt in self.options:  # Only test known options
                if opt.isProcessingMode:
                    optArgs = getattr(self.parsedArgs, opt.name)  # Get argument in a generic way
                    if isinstance(optArgs, list):  # To avoid process when the option hasn't been used
                        page = self.newWebPage(self.parsedArgs.outputDir[0], optArgs)  # Create a new page using the argument(s)
                        processMethod = getattr(page, 'process' + opt.name.title())  # Find the correct process method in a generic way
                        processMethod()  # Call the process method

    def newWebPage(self, outputDir, args):
        return WebPage(outputDir, *args)

    def newArg(self, name, narg, helpText=None):
        return Arg(name, narg, helpText)


# Untested (untestable?) code to run main when executed as a script
if __name__ == '__main__':
    wb = WebsiteBuilder()
    wb.main(*sys.argv[1:])
