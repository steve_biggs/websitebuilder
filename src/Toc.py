"""
Represents a table of contents

Copyright 2018 Steve Biggs

This file is part of websitebuilder.

websitebuilder is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

websitebuilder is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with websitebuilder.  If not, see http://www.gnu.org/licenses/.
"""


class Toc:

    def __init__(self, entries):
        self.entries = entries

    def getHtml(self):
        html = ''
        for entry in self.entries:
            html += entry.getHtml() + '\n'
        return html.strip()  # Use strip to remove the final \n as we don't want it
