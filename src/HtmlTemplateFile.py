"""
Represents a generic HTML template file

Copyright 2018 Steve Biggs

This file is part of websitebuilder.

websitebuilder is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

websitebuilder is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with websitebuilder.  If not, see http://www.gnu.org/licenses/.
"""

from HtmlFile import HtmlFile


class HtmlTemplateFile(HtmlFile):

    def __init__(self, outputDir, contentPath, title):
        super(HtmlTemplateFile, self).__init__(outputDir, contentPath)
        self.title = title

    def getPageName(self, title):
        filename = title
        if filename.istitle():
            filename = filename.lower()
        if title == 'Home':
            filename = 'index'
        if title == 'Appendix - UKPSF':
            filename = 'UKPSF'
        return filename

    def getSelfPageName(self):
        return self.getPageName(self.title)
