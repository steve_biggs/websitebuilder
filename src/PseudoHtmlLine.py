"""
Represents a single line from a pseudo-HTML file

Copyright 2018 Steve Biggs

This file is part of websitebuilder.

websitebuilder is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

websitebuilder is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with websitebuilder.  If not, see http://www.gnu.org/licenses/.
"""

import re
from TocEntry import TocEntry


class PseudoHtmlLine:

    def __init__(self, text=''):
        self.text = text
        self.tocEntry = None

    def __eq__(self, other):
        equal = False
        if isinstance(self, other.__class__):
            equal = self.text == other.text
        return equal

    def __repr__(self):
        return "PseudoHtmlLine('" + self.text + "')"

    def isBlank(self):
        return self.text is None or self.text == '' or self.text == '\n'

    def isHeader(self):
        return re.search(r'<h\d>', self.text) is not None

    def isBookmark(self):
        return self.text is not None and \
            re.search(r'<a .*name=', self.text) is not None

    def isList(self):
        return re.match(r'</?[ou]l>\n', self.text) is not None

    def openParagraph(self):
        return PseudoHtmlLine('<p>' + self.text)

    def closeParagraph(self):
        modifiedText = re.sub(r'(.*)(\n?)', r'\1</p>\2', self.text)
        return PseudoHtmlLine(modifiedText)

    def formatBookmark(self):
        newText = self.insertFormattingClasses(self.text)
        newText = self.removeTocAttribute(newText)
        return PseudoHtmlLine(newText)

    def insertFormattingClasses(self, text):
        return re.sub(r'(<a .*)(name=)', r'\1class="anchor w3-text-black" \2', text)

    def removeTocAttribute(self, text):
        return re.sub(r' toc=".*">', r'>', text)

    def captureTocEntry(self):
        if self.isBookmark():  # Only valid to do this for bookmark lines
            # e.g. <h2><a anchor="anchor" label="entry">Title</a></h2>
            anchor = re.search(r'name=["\'](.*?)["\']', self.text).group(1)
            if not anchor == 'top':  # Ignore top as this is added as part of the template
                level = int(re.search(r'<h(\d)>', self.text).group(1))
                labelMatch = re.search(r'toc=["\'](.*?)["\']', self.text)
                if labelMatch is None:
                    # e.g. <h2><a anchor="anchor">Title</a></h2>
                    labelMatch = re.search(r'<.*?><.*?>(.*?)<.*?><.*?>', self.text)
                label = labelMatch.group(1)
                self.tocEntry = TocEntry(level, anchor, label)
