"""
Represents a command line argument

Copyright 2018 Steve Biggs

This file is part of websitebuilder.

websitebuilder is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

websitebuilder is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with websitebuilder.  If not, see http://www.gnu.org/licenses/.
"""


class Arg:

    def __init__(self, name, narg, helpText=None):
        self.name = name
        self.narg = narg
        self.isProcessingMode = helpText is None
        self.assignHelpText(name, narg, helpText)

    def assignHelpText(self, name, narg, helpText):
        if helpText:
            self.help = helpText
        else:
            firstArg = name.upper() if narg == 1 else name.upper() + '1'
            secondArg = '' if narg == 1 else ' ' + name.upper() + '2 = path to ' + name + ' template file.'
            self.help = 'Build ' + name + '. ' + firstArg + ' = path to content file.' + secondArg
