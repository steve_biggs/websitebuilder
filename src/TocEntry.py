"""
Represents a single table of contents entry

Copyright 2018 Steve Biggs

This file is part of websitebuilder.

websitebuilder is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

websitebuilder is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with websitebuilder.  If not, see http://www.gnu.org/licenses/.
"""


class TocEntry:

    def __init__(self, level, anchor, label):
        self.level = level
        self.anchor = anchor
        self.label = label

    def getHtml(self):
        minLevel = 2
        if self.level < minLevel:
            raise ValueError('Level must be >= ' + str(minLevel))
        maxLevel = 3
        if self.level > maxLevel:
            raise NotImplementedError('Level > ' + str(maxLevel) + ' not implemented yet')
        return '<h' + str(self.level) + '><a class="w3-bar-item w3-button w3-hover-amber toc' + str(self.level) + '" href="#' + self.anchor + '">' + self.label + '</a></h' + str(self.level) + '>'