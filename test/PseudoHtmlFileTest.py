"""
Unit tests of PseudoHtmlFile

Copyright 2018 Steve Biggs

This file is part of websitebuilder.

websitebuilder is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

websitebuilder is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with websitebuilder.  If not, see http://www.gnu.org/licenses/.
"""

from unittest import TestCase, mock
import sys
sys.path.insert(0, '../src')
from PseudoHtmlFile import PseudoHtmlFile as HtmlFile
from PseudoHtmlBlock import PseudoHtmlBlock as Block
from PseudoHtmlLine import PseudoHtmlLine as Line
from Toc import Toc


class PseudoHtmlFileTest(TestCase):

    def setUp(self):
        self.outputDir = 'path/to/output'
        self.contentFilename = 'filename'
        self.inputExtension = '.shtml'
        self.outputExtension = '.html'

    def testInitCreatesABlankBlock(self):
        htmlFile = HtmlFile(self.outputDir, self.contentFilename)
        self.assertIsInstance(htmlFile.block, Block)

    def testInitAssignsOutputFilenameAsContentFilenameWithShtmlChangedToHtml(self):
        htmlFile = HtmlFile(self.outputDir, self.contentFilename + self.inputExtension)
        self.assertEqual(htmlFile.outputFilename, self.contentFilename + self.outputExtension)

    def testInitAssignsOutputFilenameBasedOnContentFilenameNotPAth(self):
        sourceDir = 'path/to/'
        htmlFile = HtmlFile(self.outputDir, sourceDir + self.contentFilename + self.inputExtension)
        self.assertEqual(htmlFile.outputFilename, self.contentFilename + self.outputExtension)


class PseudoHtmlFileProcessTest(TestCase):

    def setUp(self):
        self.htmlFile = FakeFile()
        self.fileContents = ["first line", "second line"]
        self.htmlFile.contents = self.fileContents

    def testProcessCallsProcessLineForEachInATwoLineFile(self):
        self.htmlFile.parsePseudoHtml()
        # Expected answer is len + 1 because first parsePseudoHtmlLine doesn't get
        # recorded (only used to step first line) and so an extra parsePseudoHtmlLine
        # is needed to parse the last line
        self.assertEqual(self.htmlFile.callCount,
                         len(self.fileContents) + 1)

    def testProcessCallsProcessLineForEachInAThreeLineFile(self):
        # Expected answer is len + 1 as above
        self.fileContents = ["first line", "second line", "etc"]
        self.htmlFile.contents = self.fileContents
        self.htmlFile.parsePseudoHtml()
        self.assertEqual(self.htmlFile.callCount,
                         len(self.fileContents) + 1)

    def testProcessCallsProcessLineFirstTimeWithFirstLine(self):
        self.htmlFile.parsePseudoHtml()
        self.assertEqual(self.htmlFile.args[0], self.fileContents[0])

    def testProcessCallsProcessLineSecondTimeWithSecondLine(self):
        self.htmlFile.parsePseudoHtml()
        self.assertEqual(self.htmlFile.args[1], self.fileContents[1])

    def testProcessCallsProcessLineExtraTimeWithBlankLine(self):
        self.htmlFile.parsePseudoHtml()
        self.assertEqual(self.htmlFile.args[2], '')


class PseudoHtmlFileProcessLineTest(TestCase):

    def setUp(self):
        self.contentPath = 'path/to/file'
        self.htmlFile = HtmlFile('path/to/output', self.contentPath)
        self.mockBlock = mock.Mock(spec=Block)
        self.htmlFile.block = self.mockBlock
        self.lineText = "first line"
        self.line = Line(self.lineText)

    def testProcessLineStepsTheBlockWithTheGivenLine(self):
        self.htmlFile.parsePseudoHtmlLine(self.lineText)
        self.mockBlock.step.assert_called_with(self.line)

    def testProcessLineProcessesTheBlock(self):
        self.htmlFile.parsePseudoHtmlLine(self.lineText)
        self.mockBlock.parsePseudoHtml.assert_called_with()

    def testProcessLineCallsStepFirst(self):
        self.htmlFile.parsePseudoHtmlLine(self.lineText)
        self.mockBlock.assert_has_calls(
            [mock.call.step(self.line),
             mock.call.parsePseudoHtml()])

    def testProcessLineReturnsTheValueReturnedByBlockProcess(self):
        mockLine = mock.Mock(spec=Line)
        mockLine.text = 'blah'
        self.mockBlock.parsePseudoHtml.return_value = mockLine
        actual = self.htmlFile.parsePseudoHtmlLine(self.lineText)
        self.assertEqual(actual, mockLine.text)

    def testProcessLineReturnsSecondBlockProcessValueSecondTime(self):
        mockFirstLine = mock.Mock(spec=Line)
        mockFirstLine.text = 'first'
        mockSecondLine = mock.Mock(spec=Line)
        mockSecondLine.text = 'second'
        self.mockBlock.parsePseudoHtml.side_effect = [mockFirstLine,
                                              mockSecondLine]
        self.htmlFile.parsePseudoHtmlLine("first line")
        actual = self.htmlFile.parsePseudoHtmlLine("second line")
        self.assertEqual(actual, mockSecondLine.text)


class PseudoHtmlFileOutputTest(TestCase):

    def setUp(self):
        self.htmlFile = HtmlFile('path/to/output', 'path/to/file')
        self.htmlFile.contents = ["first line"]
        self.htmlFile.block = mock.Mock(spec=Block)

    def testOutputIsNoneBeforeProcessing(self):
        self.assertIsNone(self.htmlFile.output)

    def testTocIsNoneBeforeProcessing(self):
        self.assertIsNone(self.htmlFile.toc)

    def testOutputIsAListAfterProcessing(self):
        self.htmlFile.parsePseudoHtml()
        self.assertIsInstance(self.htmlFile.output, list)

    def testTocIsATocAfterProcessing(self):
        self.htmlFile.captureTocEntries()
        self.assertIsInstance(self.htmlFile.toc, Toc)

    def testOutputIsOfLengthOneWhenContentsIsOfLengthOne(self):
        self.htmlFile.parsePseudoHtml()
        self.assertEqual(len(self.htmlFile.output),
                         len(self.htmlFile.contents))

    def testTextToLineReturnsNewLineWithGivenText(self):
        text = 'wibble'
        actual = self.htmlFile.textToLine(text)
        self.assertEqual(actual, Line(text))

    def testCaptureTocEntriesCallsCaptureTocEntryForEachBookmark(self):
        self.htmlFile.contents = ['bookmark', 'bookmark', 'bookmark', 'bookmark', 'bookmark']
        fakeBookmarkLine = FakeBookmarkLine()
        mockCaptureTocEntryMethod = mock.Mock()
        fakeBookmarkLine.captureTocEntry = mockCaptureTocEntryMethod
        self.htmlFile.textToLine = mock.Mock(return_value=fakeBookmarkLine)
        self.htmlFile.captureTocEntries()
        self.assertEqual(mockCaptureTocEntryMethod.call_count, len(self.htmlFile.contents))

    def testCaptureTocEntriesCallsCaptureTocEntryForEachNonBookmark(self):
        self.htmlFile.contents = ['non-bookmark', 'non-bookmark', 'non-bookmark', 'non-bookmark']
        fakeNonBookmarkLine = FakeNonBookmarkLine()
        mockCaptureTocEntryMethod = mock.Mock()
        fakeNonBookmarkLine.captureTocEntry = mockCaptureTocEntryMethod
        self.htmlFile.textToLine = mock.Mock(return_value=fakeNonBookmarkLine)
        self.htmlFile.captureTocEntries()
        self.assertEqual(mockCaptureTocEntryMethod.call_count, len(self.htmlFile.contents))

    def testTocCreatedWithCapturedTocEntriesWhenContentsContainsOneBookmark(self):
        mockBookmarkLine = mock.Mock()
        tocEntry = mock.Mock()
        mockBookmarkLine.tocEntry = tocEntry
        self.htmlFile.contents = ['bookmark']
        self.htmlFile.textToLine = mock.Mock(return_value=mockBookmarkLine)
        self.htmlFile.newToc = mock.Mock()
        self.htmlFile.captureTocEntries()
        self.htmlFile.newToc.assert_called_with([tocEntry])

    def testTocCreatedWithCapturedTocEntriesWhenContentsContainsTwoBookmarks(self):
        tocEntry1 = mock.Mock()
        tocEntry2 = mock.Mock()
        mockBookmarkLine1 = mock.Mock()
        mockBookmarkLine2 = mock.Mock()
        mockBookmarkLine1.tocEntry = tocEntry1
        mockBookmarkLine2.tocEntry = tocEntry2
        self.htmlFile.contents = ['bookmark', 'bookmark']
        self.htmlFile.textToLine = mock.Mock()
        self.htmlFile.textToLine.side_effect = [mockBookmarkLine1, mockBookmarkLine2]
        self.htmlFile.newToc = mock.Mock()
        self.htmlFile.captureTocEntries()
        self.htmlFile.newToc.assert_called_with([tocEntry1, tocEntry2])

    def testTocCreatedWithCapturedTocEntriesWhenContentsContainsTwoBookmarksAndANonBookmark(self):
        tocEntry1 = mock.Mock()
        tocEntry2 = mock.Mock()
        mockBookmarkLine1 = mock.Mock()
        mockBookmarkLine2 = mock.Mock()
        mockNonBookmarkLine = mock.Mock()
        mockBookmarkLine1.tocEntry = tocEntry1
        mockBookmarkLine2.tocEntry = tocEntry2
        mockNonBookmarkLine.tocEntry = None
        self.htmlFile.contents = ['bookmark', 'bookmark', 'non-bookmark']
        self.htmlFile.textToLine = mock.Mock()
        self.htmlFile.textToLine.side_effect = [mockBookmarkLine1, mockBookmarkLine2, mockNonBookmarkLine]
        self.htmlFile.newToc = mock.Mock()
        self.htmlFile.captureTocEntries()
        self.htmlFile.newToc.assert_called_with([tocEntry1, tocEntry2])

    def testTocContainsReturnValuesOfNewToc(self):
        self.htmlFile.contents = ['bookmark']
        self.htmlFile.textToLine = mock.Mock()
        mockToc = mock.Mock()
        self.htmlFile.newToc = mock.Mock(return_value=mockToc)
        self.htmlFile.captureTocEntries()
        self.assertEqual(self.htmlFile.toc, mockToc)

    def testOutputIsOfLengthTwoWhenContentsIsOfLengthTwo(self):
        self.htmlFile.contents = ["first line", "second line"]
        self.htmlFile.parsePseudoHtml()
        self.assertEqual(len(self.htmlFile.output),
                         len(self.htmlFile.contents))

    def testOutputContainsTheReturnValuesFromProcessLine(self):
        self.htmlFile.contents = ["first input", "second input"]
        mockProcessLineMethod = mock.Mock()
        mockProcessLineMethod.side_effect = ["zeroth output (blank)",
                                             "first output",
                                             "second output"]
        self.htmlFile.parsePseudoHtmlLine = mockProcessLineMethod
        self.htmlFile.parsePseudoHtml()
        self.assertEqual(self.htmlFile.output,
                         ["first output", "second output"])


class FakeFile(HtmlFile):

    def __init__(self):
        self.callCount = 0
        self.args = []

    def parsePseudoHtmlLine(self, line):
        self.callCount += 1
        self.args.append(line)


class FakeBookmarkLine(Line):

    def isBookmark(self):
        return True

    def captureTocEntry(self):
        pass


class FakeNonBookmarkLine(Line):

    def isBookmark(self):
        return False
