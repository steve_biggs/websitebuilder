"""
Unit tests of WebsiteBuilder

Copyright 2018 Steve Biggs

This file is part of websitebuilder.

websitebuilder is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

websitebuilder is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with websitebuilder.  If not, see http://www.gnu.org/licenses/.
"""

from unittest import TestCase, mock
import sys
sys.path.insert(0, '../src')
from WebsiteBuilder import WebsiteBuilder
from WebPage import WebPage as Page
from Arg import Arg


class WebsiteBuilderMainTest(TestCase):

    def setUp(self):
        self.wb = WebsiteBuilder()
        self.mockParsedArgs = mock.Mock()
        self.mockArgumentParser = mock.Mock()
        self.mockArgumentParser.parse_known_args = mock.Mock(return_value=(self.mockParsedArgs, mock.Mock()))
        self.mockCreateParserMethod = mock.Mock(return_value=self.mockArgumentParser)
        self.wb.createParser = self.mockCreateParserMethod

    def testMainCallsCreateParser(self):
        self.wb.main()
        self.assertTrue(self.mockCreateParserMethod.called)

    def testMainCallsParseKnownArgsWithOneGivenArg(self):
        arg = 'something'
        self.wb.main(arg)
        self.mockArgumentParser.parse_known_args.assert_called_with((arg,))

    def testMainCallsParseArgsWithTwoGivenArgs(self):
        arg1 = 'something'
        arg2 = 'another'
        self.wb.main(arg1, arg2)
        self.mockArgumentParser.parse_known_args.assert_called_with((arg1, arg2))

    def testParsedArgsIsNoneBeforeParsing(self):
        self.assertIsNone(self.wb.parsedArgs)

    def testParsedArgsStoredInObjectAfterParsing(self):
        self.wb.main()
        self.assertIs(self.wb.parsedArgs, self.mockParsedArgs)

    def testMainCallsBuildTarget(self):
        mockBuildTargetMethod = mock.Mock()
        self.wb.buildTarget = mockBuildTargetMethod
        self.wb.main()
        self.assertTrue(mockBuildTargetMethod.called)


class WebsiteBuilderParseArgsTest(TestCase):

    def setUp(self):
        wb = WebsiteBuilder()
        self.parser = wb.createParser()
        self.filename = 'my-page-content.shtml'

    def testParseArgsAcceptsOutputDirOption(self):
        outputDir = 'contentPath/to/outputDir'
        parsedArgs, dummy = self.parser.parse_known_args(['--outputDir', outputDir])
        self.assertEqual(parsedArgs.outputDir, [outputDir])

    def testParseArgsIgnoresArgsAfterFirstWithOutputDirOption(self):
        outputDir = 'contentPath/to/outputDir'
        parsedArgs, dummy = self.parser.parse_known_args(['--outputDir', outputDir, 'something-else', 'another-thing'])
        self.assertEqual(parsedArgs.outputDir, [outputDir])

    def testParseArgsAcceptsContentOptionToBuildContent(self):
        parsedArgs, dummy = self.parser.parse_known_args(['--content', self.filename])
        self.assertEqual(parsedArgs.content, [self.filename])

    def testParseArgsIgnoresArgsAfterFirstWithContentOption(self):
        parsedArgs, dummy = self.parser.parse_known_args(['--content', self.filename, 'something-else', 'another-thing'])
        self.assertEqual(parsedArgs.content, [self.filename])

    def testParseArgsAcceptsPageOptionToBuildPage(self):
        template = 'my-super-template.html'
        parsedArgs, dummy = self.parser.parse_known_args(['--page', self.filename, template])
        self.assertEqual(parsedArgs.page, [self.filename, template])

    def testParseArgsIgnoresArgsAfterSecondWithPageOption(self):
        template = 'my-super-template.html'
        parsedArgs, dummy = self.parser.parse_known_args(['--page', self.filename, template, 'something-else', 'another-thing'])
        self.assertEqual(parsedArgs.page, [self.filename, template])

    def testParseArgsAcceptsBarOptionToBuildBar(self):
        template = 'my-bar-template.html'
        parsedArgs, dummy = self.parser.parse_known_args(['--bar', self.filename, template])
        self.assertEqual(parsedArgs.bar, [self.filename, template])

    def testParseArgsAcceptsDropOptionToBuildDrop(self):
        template = 'my-drop-template.html'
        parsedArgs, dummy = self.parser.parse_known_args(['--drop', self.filename, template])
        self.assertEqual(parsedArgs.drop, [self.filename, template])


class WebsiteBuilderBuildTargetTest(TestCase):

    def setUp(self):
        self.wb = WebsiteBuilder()
        self.mockParsedArgs = mock.Mock()
        self.outputDir = 'path/to/output'
        self.mockParsedArgs.outputDir = [self.outputDir]
        self.wb.parsedArgs = self.mockParsedArgs
        self.mockWebPage = mock.Mock()
        self.mockNewPageMethod = mock.Mock(return_value=self.mockWebPage)
        self.wb.newWebPage = self.mockNewPageMethod
        self.contentPath = 'path/to/content'
        self.templatePath = 'path/to/template'

    def testBuildContentCallsNewPageWithGivenFilename(self):
        args = [self.contentPath]
        self.mockParsedArgs.content = args
        self.wb.buildTarget()
        self.mockNewPageMethod.assert_called_with(self.outputDir, args)

    def testBuildPageCallsNewPageWithGivenFilename(self):
        args = [self.contentPath, self.templatePath]
        self.mockParsedArgs.page = args
        self.wb.buildTarget()
        self.mockNewPageMethod.assert_called_with(self.outputDir, args)

    def testBuildBarCallsNewPageWithGivenFilename(self):
        args = [self.contentPath, self.templatePath]
        self.mockParsedArgs.bar = args
        self.wb.buildTarget()
        self.mockNewPageMethod.assert_called_with(self.outputDir, args)

    def testBuildDropCallsNewPageWithGivenFilename(self):
        args = [self.contentPath, self.templatePath]
        self.mockParsedArgs.drop = args
        self.wb.buildTarget()
        self.mockNewPageMethod.assert_called_with(self.outputDir, args)

    def testBuildContentCallsPageProcessContent(self):
        self.mockParsedArgs.content = [self.contentPath]
        self.wb.buildTarget()
        self.assertTrue(self.mockWebPage.processContent.called)

    def testBuildPageCallsPageProcessPage(self):
        self.mockParsedArgs.page = [self.contentPath, self.templatePath]
        self.wb.buildTarget()
        self.assertTrue(self.mockWebPage.processPage.called)

    def testBuildBarCallsPageProcessBar(self):
        self.mockParsedArgs.bar = [self.contentPath, self.templatePath]
        self.wb.buildTarget()
        self.assertTrue(self.mockWebPage.processBar.called)

    def testBuildDropCallsPageProcessDrop(self):
        self.mockParsedArgs.drop = [self.contentPath, self.templatePath]
        self.wb.buildTarget()
        self.assertTrue(self.mockWebPage.processDrop.called)

    def testBuildOutputDirDoesNotCreateANewPageWithTheOutputDir(self):
        self.mockParsedArgs.outputDir = ['path/to/outputDir']
        self.wb.buildTarget()
        self.assertFalse(self.mockNewPageMethod.called)


class WebsiteBuilderTest(TestCase):

    def testNewPageReturnsAPage(self):
        wb = WebsiteBuilder()
        page = wb.newWebPage('path/to/output', ['path/to/content'])
        self.assertIsInstance(page, Page)

    def testNewArgReturnsAnArg(self):
        wb = WebsiteBuilder()
        arg = wb.newArg('name', 1)
        self.assertIsInstance(arg, Arg)

    def testInitSetsUpOutputDirCorrectly(self):
        wb = WebsiteBuilder()
        wb.newArg = mock.Mock()
        expectedCall = [
            mock.call('outputDir', 1, helpText='Directory in which to place output files. OUTPUTDIR = path to output directory.')]
        wb.__init__()
        wb.newArg.assert_has_calls(expectedCall)