"""
Unit tests of TocEntry

Copyright 2018 Steve Biggs

This file is part of websitebuilder.

websitebuilder is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

websitebuilder is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with websitebuilder.  If not, see http://www.gnu.org/licenses/.
"""

from unittest import TestCase
import sys
sys.path.insert(0, '../src')
from TocEntry import TocEntry


class TocEntryTest(TestCase):

    def testGetHtmlReturnsCorrectHtmlForLevel2Heading(self):
        entry = TocEntry(2, 'anchor', 'label')
        self.assertEqual(entry.getHtml(), '<h2><a class="w3-bar-item w3-button w3-hover-amber toc2" href="#anchor">label</a></h2>')

    def testGetHtmlReturnsCorrectHtmlForAnotherLevel2Heading(self):
        entry = TocEntry(2, 'link', 'text')
        self.assertEqual(entry.getHtml(), '<h2><a class="w3-bar-item w3-button w3-hover-amber toc2" href="#link">text</a></h2>')

    def testGetHtmlReturnsCorrectHtmlForLevel3Heading(self):
        entry = TocEntry(3, 'anchor', 'label')
        self.assertEqual(entry.getHtml(), '<h3><a class="w3-bar-item w3-button w3-hover-amber toc3" href="#anchor">label</a></h3>')

    def testGetHtmlReturnsCorrectHtmlForAnotherLevel3Heading(self):
        entry = TocEntry(3, 'link', 'text')
        self.assertEqual(entry.getHtml(), '<h3><a class="w3-bar-item w3-button w3-hover-amber toc3" href="#link">text</a></h3>')

    def testGetHtmlRaisesValueErrorForLevel1(self):
        entry = TocEntry(1, 'link', 'text')
        with self.assertRaises(ValueError):
            entry.getHtml()

    def testGetHtmlRaisesValueErrorWithErrorMessageForLevel1(self):
        entry = TocEntry(1, 'link', 'text')
        with self.assertRaises(ValueError) as cm:
            entry.getHtml()
        self.assertEqual(cm.exception.args[0], 'Level must be >= 2')

    def testGetHtmlRaisesNotImplementedErrorForLevel4(self):
        entry = TocEntry(4, 'link', 'text')
        with self.assertRaises(NotImplementedError):
            entry.getHtml()

    def testGetHtmlRaisesNotImplementedErrorWithMessageForLevel3(self):
        entry = TocEntry(4, 'link', 'text')
        with self.assertRaises(NotImplementedError) as cm:
            entry.getHtml()
        self.assertEqual(cm.exception.args[0], 'Level > 3 not implemented yet')

