"""
Unit tests of Arg

Copyright 2018 Steve Biggs

This file is part of websitebuilder.

websitebuilder is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

websitebuilder is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with websitebuilder.  If not, see http://www.gnu.org/licenses/.
"""

from unittest import TestCase
import sys
sys.path.insert(0, '../src')
from Arg import Arg


class ArgTest(TestCase):

    def testContentHelpIsGeneratedCorrectly(self):
        arg = Arg('content', 1)
        self.assertEqual(arg.help, 'Build content. CONTENT = path to content file.')

    def testPageHelpIsGeneratedCorrectly(self):
        arg = Arg('page', 2)
        self.assertEqual(arg.help, 'Build page. PAGE1 = path to content file. PAGE2 = path to page template file.')

    def testBarHelpIsGeneratedCorrectly(self):
        arg = Arg('bar', 2)
        self.assertEqual(arg.help, 'Build bar. BAR1 = path to content file. BAR2 = path to bar template file.')

    def testDropHelpIsGeneratedCorrectly(self):
        arg = Arg('drop', 2)
        self.assertEqual(arg.help, 'Build drop. DROP1 = path to content file. DROP2 = path to drop template file.')

    def testOutputDirHelpIsAsGiven(self):
        helpText = 'Directory in which to place output files. OUTPUTDIR = path to output directory.'
        arg = Arg('outputDir', 1, helpText=helpText)
        self.assertEqual(arg.help, helpText)
